package org.neo4j.examples.dijkstra;


import java.util.Date;
import java.util.HashMap;
import java.util.PriorityQueue;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;


public class DijkstraShortestPathConstantWeight{

	private GraphDatabaseService graphDb;
	protected static int wasRemoved = -1;
	
	public DijkstraShortestPathConstantWeight(GraphDatabaseService graphDb) {
		this.graphDb = graphDb;
	}
	
	public int shortestPath(Node source, Node target, Date time) {
		PriorityQueue<Entry> queue = new PriorityQueue<Entry>();
		HashMap<Integer, Integer> wasTraversed = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> parents = new HashMap<Integer, Integer>();
		Entry removed = null;
		int tid = (int) target.getId();
		int t = DateUtils.dateToMilli(time);
		
		init(source, target, queue, parents, t);
		
		while(!queue.isEmpty()){
			removed = queue.poll();
			//System.out.println("removed: " + removed.getId() + " tt: " + removed.getTt());
			wasTraversed.put(removed.getId(), wasRemoved);	
			parents.put(removed.getId(), removed.getParent());
			
			if(removed.getId() == tid){
				return removed.getTt();
			}
			
			expandVertex(target, removed, wasTraversed, queue);
		}
		
		throw new PathNotFoundException();
	}
	
	public void init(Node source, Node target, PriorityQueue<Entry> queue, 
			HashMap<Integer, Integer> parents, int t){
		int sid = (int) source.getId();
		
		queue.offer(new Entry(sid, 
				0, 
				t, 
				-1));

		parents.put(sid, -1);
	}
	
	public void expandVertex(Node target, Entry removed, HashMap<Integer, Integer> wasTraversed, 
			PriorityQueue<Entry> queue){
		
		Traverser nodesTraverser = getNeighbors( graphDb.getNodeById(removed.getId()) );
		
		for (Path nodedPath : nodesTraverser ) {
			int vid = (int) nodedPath.endNode().getId();
			int weight = (Integer) nodedPath.lastRelationship().getProperty("weight");
			int at = getArrival(removed.getAt(), weight);
			int tt = removed.getTt() + weight;
			Entry newEntry = new Entry(	vid, 
													tt, 
													at, 
													removed.getId());
			if(!wasTraversed.containsKey(vid)){					
				queue.offer(newEntry);
				wasTraversed.put(newEntry.getId(), newEntry.getTt());
			}else{
				int cost = wasTraversed.get(vid);
				if(cost != wasRemoved){
					if(cost>newEntry.getTt()){
						queue.remove(newEntry);
						queue.offer(newEntry);
						wasTraversed.remove(newEntry.getId());
						wasTraversed.put(newEntry.getId(), newEntry.getTt());
					}
				}
			}
		}
	}
	
	public int getArrival(int dt, int tt) {
		int maxTime = 24 * 60 * 60 * 1000;
		int at = dt + tt;
		at = at % maxTime;
		return at;
	}
	
	enum RelTypes implements RelationshipType
	{
	   neighbor
	}
	
	private static org.neo4j.graphdb.traversal.Traverser getNeighbors(
	        final Node node )
	{
	    TraversalDescription td = Traversal.description()
	            .relationships( RelTypes.neighbor, Direction.OUTGOING )
	            .evaluator(Evaluators.fromDepth(1))                     
	            .evaluator(Evaluators.toDepth(1))
	            .evaluator( Evaluators.excludeStartPosition() );
	    return td.traverse( node );
	}

}
