package org.neo4j.examples.dijkstra;

import org.neo4j.graphalgo.CommonEvaluators;
import org.neo4j.graphalgo.CostEvaluator;
import org.neo4j.graphalgo.EstimateEvaluator;
import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphalgo.WeightedPath;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipExpander;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.Traversal;

public class DijkstraNativeNeo4j {
	
	public static void main(String[] args) {
		
	  GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("/home/camila/mobme/brasil/neo4j/graph");
	  RelationshipExpander expander = Traversal.expanderForTypes(
              ExampleGraphService.MyDijkstraTypes.neighbor, Direction.OUTGOING );
	  CostEvaluator<Double> costEvaluator = CommonEvaluators.doubleCostEvaluator("weight");
	 
	  //dijkstra
	 // PathFinder<WeightedPath> pathFinder = GraphAlgoFactory.dijkstra(expander, costEvaluator);
	  
	  //astar
	  EstimateEvaluator<Double> estimateEval = CommonEvaluators.geoEstimateEvaluator("lat", "long" );
	  PathFinder<WeightedPath> pathFinder = GraphAlgoFactory.aStar(expander, costEvaluator, estimateEval);
	  
	  Node start = graphDb.getNodeById(2085560);
      Node end = graphDb.getNodeById(1986564);
      long beginTime = System.currentTimeMillis();
      WeightedPath path = pathFinder.findSinglePath( start, end );
      long endTime = System.currentTimeMillis();
      System.out.println("time:" + (endTime - beginTime));
      System.out.println("cost:" + path.weight());
      
//      for ( Node node : path.nodes() )
//      {
//          System.out.println( node.getProperty( "vid" ) );
//      }
      
      graphDb.shutdown();
	}
}
