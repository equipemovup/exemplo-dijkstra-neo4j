package org.neo4j.examples.dijkstra;

import java.text.ParseException;
import java.util.Date;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class DijkstraShortestPathConstantWeightTest {

	public static void main(String[] args) throws ParseException {
		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("/home/camila/mobme-test/neo4j/graph");
		DijkstraShortestPathConstantWeight dij = new DijkstraShortestPathConstantWeight(graphDb);
	
		Node start = graphDb.getNodeById(43513);
	    Node end = graphDb.getNodeById(364359);
	    Date date = DateUtils.parseDate(0, 550, 0);
	    long beginTime = System.currentTimeMillis();
	    System.out.println("start");
	    int cost = dij.shortestPath(start, end, date);
	    long endTime = System.currentTimeMillis();
	    System.out.println("time:" + (endTime - beginTime));
	    System.out.println("cost:" + cost);
	
	}
	
}
