package org.neo4j.examples.dijkstra;


public class Entry implements Comparable<Object>{
    private int id;
    private int tt;
    private int at;
    private int parent;
         
    public Entry(int id, int tt, int at, int parent) {
    	this.id = id;
    	this.tt = tt;
    	this.at = at;
    	this.parent = parent;
	}

	public boolean equals(Entry o){
        return this.id == o.id;
    }
 
	public int compareTo(Object o) {
		return new Integer(tt).compareTo(((Entry)o).tt);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTt() {
		return tt;
	}

	public void setTt(int tt) {
		this.tt = tt;
	}

	public int getAt() {
		return at;
	}

	public void setAt(int at) {
		this.at = at;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}
}
